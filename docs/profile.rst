.. _profile:

Profile
=======

From your profile page, you can manage tokens that can be used to access the :ref:`API`.
You can access our profile by clicking on your name in the upper right corner.

.. image:: _static/access_profile.png

.. _token-generation:

Generate a personal access token
--------------------------------

1. Enter a description for the token and click on **Generate token**.

   .. image:: _static/profile.png

2. Click on the icon on the left of the generated token to copy it to the clipboard.
   Save it to a safe place. You won't be able to see the token again! Only the JWT id is saved to the database.

   .. image:: _static/token_copied.png


Revoke a personal access token
------------------------------

1. Click on the trash icon on the left of the JWT id on your profile page. A confirmation window will open.

   .. image:: _static/revoke_token.png

2. Click **OK** to revoke the token. Or **Cancel**.

   .. image:: _static/token_revoked.png
