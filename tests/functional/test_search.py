# -*- coding: utf-8 -*-
"""
tests.functional.test_search
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This module defines search tests.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import elasticsearch
import pytest
from app import search


class MyModel:
    def __init__(self, id, name, description=""):
        self.id = id
        self.name = name
        self.description = description

    def to_dict(self, recursive=False):
        return {"id": self.id, "name": self.name, "description": self.description}


def test_add_to_index(db):
    model1 = MyModel(2, "foo", "This is a test")
    search.add_to_index("index-test", model1.to_dict())
    res = db.app.elasticsearch.get(index="index-test", id=2)
    assert res["_source"] == {"name": "foo", "description": "This is a test"}


def test_remove_from_index(db):
    model1 = MyModel(3, "hello world!")
    search.add_to_index("index-test", model1.to_dict())
    res = db.app.elasticsearch.search(index="index-test", q="*")
    assert res["hits"]["total"]["value"] == 1
    search.remove_from_index("index-test", model1.id)
    res = db.app.elasticsearch.search(index="index-test", q="*")
    assert res["hits"]["total"]["value"] == 0


def test_remove_from_index_non_existing():
    model1 = MyModel(1, "hello world!")
    with pytest.raises(elasticsearch.NotFoundError):
        search.remove_from_index("index-test", model1.id)


def test_query_index():
    model1 = MyModel(1, "Python", "Python is my favorite language")
    search.add_to_index("index-test", model1.to_dict())
    model1 = MyModel(2, "Java", "Your should switch to Python!")
    search.add_to_index("index-test", model1.to_dict())
    # Test query all
    ids, total = search.query_index("index-test", "*")
    assert sorted(ids) == [1, 2]
    assert total == 2
    # Test query string
    ids, total = search.query_index("index-test", "java")
    assert ids == [2]
    assert total == 1
    ids, total = search.query_index("index-test", "python")
    assert sorted(ids) == [1, 2]
    # Test query specific field
    ids, total = search.query_index("index-test", "name:python")
    assert ids == [1]
    # Test query sort
    ids, total = search.query_index("index-test", "*", sort="name.keyword")
    assert ids == [2, 1]


def test_update_document(db):
    # Create a document
    index = "index-test"
    id = 4
    name = "a name"
    description = "just an example"
    model = MyModel(id, name, description)
    search.add_to_index(index, model.to_dict())
    res = db.app.elasticsearch.get(index="index-test", id=id)
    assert res["_source"] == {"name": name, "description": description}
    # Update the name field (description doesn't change)
    new_name = "new name"
    search.update_document(index, id, {"name": new_name})
    res = db.app.elasticsearch.get(index=index, id=id)
    assert res["_source"] == {"name": new_name, "description": description}
