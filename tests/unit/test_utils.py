# -*- coding: utf-8 -*-
"""
tests.unit.test_utils
~~~~~~~~~~~~~~~~~~~~~

This module defines utils tests.

:copyright: (c) 2018 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import pytest
from pathlib import Path
from app import utils


class TestUniqueFilename:
    def test_no_file(self, tmpdir):
        p = tmpdir.join("test.xlsx")
        assert utils.unique_filename(p) == Path(p)

    def test_one_not_related_file(self, tmpdir):
        p = tmpdir.join("test.xlsx")
        tmpdir.join("foo.xlsx").write("foo")
        assert utils.unique_filename(p) == Path(p)

    def test_one_file(self, tmpdir):
        p = tmpdir.join("test.xlsx")
        p.write("Hello")
        assert utils.unique_filename(p) == Path(tmpdir.join("test-1.xlsx"))

    def test_several_files(self, tmpdir):
        p = tmpdir.join("test.xlsx")
        p.write("Hello")
        tmpdir.join("test-1.xlsx").write("foo")
        tmpdir.join("test-2.xlsx").write("foo")
        assert utils.unique_filename(p) == Path(tmpdir.join("test-3.xlsx"))

    def test_first_available(self, tmpdir):
        p = tmpdir.join("test.xlsx")
        p.write("Hello")
        tmpdir.join("test-2.xlsx").write("foo")
        assert utils.unique_filename(p) == Path(tmpdir.join("test-1.xlsx"))

    def test_no_extension(self, tmpdir):
        p = tmpdir.join("test")
        p.write("Hello")
        assert utils.unique_filename(p) == Path(tmpdir.join("test-1"))


@pytest.mark.parametrize(
    "input,expected",
    [
        ([], ""),
        (["foo"], "foo"),
        (["foo", "bar"], "foo"),
        ("hello", "hello"),
        ("", ""),
    ],
)
def test_attribute_to_string(input, expected):
    assert utils.attribute_to_string(input) == expected
