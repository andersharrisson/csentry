# -*- coding: utf-8 -*-
"""
tests.unit.test_fields
~~~~~~~~~~~~~~~~~~~~~~

This module defines fields tests.

:copyright: (c) 2018 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import pytest
from wtforms.form import Form
from app.fields import yaml, YAMLField


class MyForm(Form):
    vars = YAMLField("Ansible vars")


def test_vault_yaml_tag_load():
    s = """foo: !vault |
              $ANSIBLE_VAULT;1.1;AES256
              31333561643032383935666363366337303435363132373238313334663563346164613433616231
              3464663834343564663638613062386366303836646136360a343231373731656261303830363837
              63636137336163383637383135643065306436306365343136373138393762366534346161316633
              3166363036616162620a346536663132343137663464653663383163646239313537316537626165
              3839
    """
    value = yaml.safe_load(s)
    assert value == {
        "foo": {
            "__ansible_vault": """$ANSIBLE_VAULT;1.1;AES256
31333561643032383935666363366337303435363132373238313334663563346164613433616231
3464663834343564663638613062386366303836646136360a343231373731656261303830363837
63636137336163383637383135643065306436306365343136373138393762366534346161316633
3166363036616162620a346536663132343137663464653663383163646239313537316537626165
3839
"""
        }
    }


@pytest.mark.parametrize(
    "text_input,expected",
    [
        ("foo: hello", {"foo": "hello"}),
        ("foo:\n  - a\n  - b", {"foo": ["a", "b"]}),
        ("", None),
        ("   ", None),
    ],
)
def test_yamlfield_process_formdata(text_input, expected):
    form = MyForm()
    YAMLField.process_formdata(form.vars, [text_input])
    assert form.vars.data == expected


def test_yamlfield_process_formdata_invalid_yaml():
    form = MyForm()
    with pytest.raises(ValueError, match="This field contains invalid YAML"):
        YAMLField.process_formdata(form.vars, ["foo: hello: world"])


@pytest.mark.parametrize("text_input", ("foo", "- a\n- b"))
def test_yamlfield_process_formdata_non_dict(text_input):
    form = MyForm()
    with pytest.raises(
        ValueError, match="This field shall only contain key-value-pairs"
    ):
        YAMLField.process_formdata(form.vars, [text_input])
