# -*- coding: utf-8 -*-
"""
app.settings
~~~~~~~~~~~~

This module implements the app default settings.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import base64
import os
from pathlib import Path
from datetime import timedelta

SQLALCHEMY_DATABASE_URI = os.environ.get(
    "SQLALCHEMY_DATABASE_URI", "postgresql://ics:icspwd@postgres/csentry_db"
)
SQLALCHEMY_TRACK_MODIFICATIONS = False
BOOTSTRAP_SERVE_LOCAL = True
SECRET_KEY = (
    base64.b64decode(os.environ.get("SECRET_KEY", ""))
    or b"\x0d\x11{\xd3\x13$\xeeel\xa6\xfb\x1d~\xfd\xb2\x9d\x16\x00\xfb5\xd64\xd4\xe0"
)

JWT_BLACKLIST_ENABLED = True
JWT_BLACKLIST_TOKEN_CHECKS = ["access", "refresh"]
JWT_ACCESS_TOKEN_EXPIRES = timedelta(hours=12)

SESSION_TYPE = "redis"
SESSION_REDIS_URL = "redis://redis:6379/0"
CACHE_TYPE = "redis"
CACHE_REDIS_URL = "redis://redis:6379/1"
RQ_REDIS_URL = "redis://redis:6379/2"

ELASTICSEARCH_URL = "http://elasticsearch:9200"
ELASTICSEARCH_INDEX_SUFFIX = "-dev"
# Shall only be set to "true" for testing to make
# documents visible for search immediately
# https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-refresh.html
ELASTICSEARCH_REFRESH = "false"

LDAP_HOST = os.environ.get("LDAP_HOST", "esss.lu.se")
LDAP_PORT = int(os.environ.get("LDAP_PORT", 636))
LDAP_USE_SSL = os.environ.get("LDAP_USE_SSL", "true").lower() == "true"
LDAP_BASE_DN = os.environ.get("LDAP_BASE_DN", "DC=esss,DC=lu,DC=se")
LDAP_USER_DN = os.environ.get("LDAP_USER_DN", "")
LDAP_GROUP_DN = os.environ.get("LDAP_GROUP_DN", "")
LDAP_BIND_USER_DN = os.environ.get("LDAP_BIND_USER_DN", "ldapuser")
LDAP_BIND_USER_PASSWORD = os.environ.get("LDAP_BIND_USER_PASSWORD", "secret")
LDAP_USER_RDN_ATTR = os.environ.get("LDAP_USER_RDN_ATTR", "cn")
LDAP_USER_LOGIN_ATTR = os.environ.get("LDAP_USER_LOGIN_ATTR", "sAMAccountName")
LDAP_ALWAYS_SEARCH_BIND = (
    os.environ.get("LDAP_ALWAYS_SEARCH_BIND", "true").lower() == "true"
)
LDAP_USER_OBJECT_FILTER = os.environ.get(
    "LDAP_USER_OBJECT_FILTER", "(samAccountType=805306368)"
)
LDAP_GROUP_OBJECT_FILTER = os.environ.get("LDAP_GROUP_OBJECT_FILTER", "")
LDAP_USER_SEARCH_SCOPE = os.environ.get("LDAP_USER_SEARCH_SCOPE", "SUBTREE")
LDAP_GROUP_SEARCH_SCOPE = os.environ.get("LDAP_GROUP_SEARCH_SCOPE", "SUBTREE")
LDAP_GROUP_MEMBERS_ATTR = os.environ.get("LDAP_GROUP_MEMBERS_ATTR", "member")
# The following variables should be a list
# Can be passed as space separated string
LDAP_GET_USER_ATTRIBUTES = os.environ.get(
    "LDAP_GET_USER_ATTRIBUTES", "cn sAMAccountName mail"
).split()
LDAP_GET_GROUP_ATTRIBUTES = os.environ.get("LDAP_GET_GROUP_ATTRIBUTES", "cn").split()

# Mapping between CSEntry groups and LDAP groups
# The generic "network" group is automatically added based
# on all CSENTRY_NETWORK_SCOPES_LDAP_GROUPS
CSENTRY_LDAP_GROUPS = {
    "admin": ["ICS Control System Infrastructure group"],
    "auditor": ["ICS Control System Infrastructure group"],
    "inventory": ["ICS Employees", "ICS Consultants"],
}
# Network scopes the user has access to based on LDAP groups
# Admin users have access to all network scopes (even if not defined here)
CSENTRY_NETWORK_SCOPES_LDAP_GROUPS = {
    "TechnicalNetwork": ["ICS Employees", "ICS Consultants"],
    "LabNetworks": ["ICS Employees", "ICS Consultants"],
}
# List of network scopes where users can create a VM
ALLOWED_VM_CREATION_NETWORK_SCOPES = ["LabNetworks"]
# List of network scopes where users can set the boot profile for physical machines
ALLOWED_SET_BOOT_PROFILE_NETWORK_SCOPES = ["LabNetworks"]
# List of device types for which the boot profile can be set
ALLOWED_SET_BOOT_PROFILE_DEVICE_TYPES = ["PhysicalMachine", "MTCA-AMC"]
# List of existing boot profiles
# Shall be kept in sync with the ics-ans-role-autoinstall Ansible role
AUTOINSTALL_BOOT_PROFILES = ["localboot", "default", "cct", "LCR", "thinclient"]

NETWORK_DEFAULT_PREFIX = 24
# ICS Ids starting with this prefix are considered temporary and can be changed
# (waiting for a real label to be assigned)
# WARNING: This is defined here as a global settings but should not be changed!
TEMPORARY_ICS_ID = "ZZ"

# CSENTRY MAC organizationally unique identifier
# This is a locally administered address
MAC_OUI = "02:42:42"

DOCUMENTATION_URL = "http://ics-infrastructure.pages.esss.lu.se/csentry/index.html"
# Shall be set to staging|production|development
CSENTRY_ENVIRONMENT = "staging"

# Maximum number of elements returned per page by an API call
MAX_PER_PAGE = 100

AWX_URL = "https://torn.tn.esss.lu.se"
# AWX dynamic inventory source to update
# Use the id because resource.update requires a number
AWX_INVENTORY_SOURCE = 41
# AWX job templates
AWX_CORE_SERVICES_UPDATE = "ics-ans-core @ DHCP test"
# Shall be set to job or workflow_job
# Assumed to be job if the variable is not defined
AWX_CORE_SERVICES_UPDATE_RESOURCE = "job"
AWX_CREATE_VM = "deploy-vm-in-proxmox"
AWX_CREATE_VIOC = "deploy-vm-in-proxmox"
AWX_POST_INSTALL = {
    "VIOC": {"esss.lu.se": "", "tn.esss.lu.se": "", "cslab.esss.lu.se": ""},
    "VM": {
        "esss.lu.se": "",
        "tn.esss.lu.se": "",
        "cslab.esss.lu.se": "customize-LabVM",
    },
}
AWX_SET_NETWORK_BOOT_PROFILE = "deploy-autoinstall_server@setboot"

AWX_JOB_ENABLED = False
AWX_VM_CREATION_ENABLED = False

VM_CORES_CHOICES = [1, 2, 4, 6, 8, 24]
VM_MEMORY_CHOICES = [2, 4, 8, 16, 32, 128]
VM_DISK_CHOICES = [15, 50, 100, 250]
VM_OSVERSION_CHOICES = ["centos7", "windows10", "windows2016"]
VIOC_CORES_CHOICES = [1, 2, 4]
VIOC_MEMORY_CHOICES = [2, 4, 8]
VIOC_DISK_CHOICES = [15, 50, 100, 250]
VIOC_OSVERSION_CHOICES = ["centos7"]

# Sentry integration
# Leave to empty string to disable sentry integration
SENTRY_DSN = os.environ.get("SENTRY_DSN", "")

# Static local files
CSENTRY_STATIC_DIR = Path(__file__).parent / "static"
CSENTRY_STATIC_FILES = CSENTRY_STATIC_DIR / "files"

RQ_DEFAULT_TIMEOUT = 1800
