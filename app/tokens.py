# -*- coding: utf-8 -*-
"""
app.api.tokens
~~~~~~~~~~~~~~

This module implements helper functions to manipulate JWT.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import sqlalchemy as sa
from datetime import datetime
from flask import current_app, g, flash, redirect, request
from flask_login import user_loaded_from_request
from flask_login import login_url
from flask_jwt_extended import decode_token, create_access_token
from flask_jwt_extended.exceptions import (
    NoAuthorizationError,
    InvalidHeaderError,
    WrongTokenError,
    RevokedTokenError,
    JWTExtendedException,
)
from .extensions import db, jwt, login_manager
from . import models, utils


@user_loaded_from_request.connect
def user_loaded_from_request(app, user=None):
    g.login_via_request = True


@login_manager.unauthorized_handler
def unauthorized():
    """Called when the user is required to log in."""
    if (
        request.path.startswith("/api")
        or request.accept_mimetypes.best == "application/json"
    ):
        # API request
        # If this method is called, load_user_from_request returned None
        # Either an exception was raised by decode_jwt_from_request
        # or None was returned by models.User.query.get(int(identity))
        # Let decode_jwt_from_request raise an exception again
        # or raise CSEntryError
        jwt_data = decode_jwt_from_request(request, request_type="access")
        identity = jwt_data[current_app.config["JWT_IDENTITY_CLAIM"]]
        raise utils.CSEntryError(f"Invalid indentity '{identity}'", status_code=403)
    else:
        # browser request
        flash("Please log in to access this page.", "info")
        redirect_url = login_url("user.login", next_url=request.url)
        return redirect(redirect_url)


def decode_jwt_from_headers(request):
    header_name = current_app.config["JWT_HEADER_NAME"]
    header_type = current_app.config["JWT_HEADER_TYPE"]
    # Verify we have the auth header
    jwt_header = request.headers.get(header_name, None)
    if not jwt_header:
        raise NoAuthorizationError(f"Missing {header_name} Header")
    # Make sure the header is in a valid format that we are expecting, ie
    # <HeaderName>: <HeaderType(optional)> <JWT>
    parts = jwt_header.split()
    if not header_type:
        if len(parts) != 1:
            msg = f"Bad {header_name} header. Expected value '<JWT>'"
            raise InvalidHeaderError(msg)
        encoded_token = parts[0]
    else:
        if parts[0] != header_type or len(parts) != 2:
            msg = f"Bad {header_name} header. Expected value '{header_type} <JWT>'"
            raise InvalidHeaderError(msg)
        encoded_token = parts[1]
    return decode_token(encoded_token)


def decode_jwt_from_request(request, request_type):
    decoded_token = decode_jwt_from_headers(request)
    # Make sure the type of token we received matches the request type we expect
    if decoded_token["type"] != request_type:
        raise WrongTokenError(f"Only {request_type} tokens can access this endpoint")
    if is_token_in_blacklist(decoded_token):
        raise RevokedTokenError("Token has been revoked")
    return decoded_token


@login_manager.request_loader
def load_user_from_request(request):
    """User loader callback using JWT from the headers

    Return a user object or None if the user doesn't exist.
    """
    try:
        jwt_data = decode_jwt_from_request(request, request_type="access")
    except JWTExtendedException:
        return None
    identity = jwt_data[current_app.config["JWT_IDENTITY_CLAIM"]]
    return models.User.query.get(int(identity))


@jwt.user_loader_callback_loader
def user_loader_callback(identity):
    """User loader callback for flask-jwt-extended

    :param str identity: identity from the token (user_id)
    :returns: corresponding user object or None
    """
    return models.User.query.get(int(identity))


@jwt.token_in_blacklist_loader
def is_token_in_blacklist(decoded_token):
    """Token blacklist loader for flask-jwt-extended

    All created tokens are added to the database. If a token is not found
    in the database, it is considered blacklisted / revoked.
    """
    jti = decoded_token["jti"]
    try:
        models.Token.query.filter_by(jti=jti).one()
    except sa.orm.exc.NoResultFound:
        return True
    return False


def generate_access_token(identity, fresh=False, expires_delta=None, description=None):
    """Create a new access token and store it in the database"""
    token = create_access_token(identity, fresh=fresh, expires_delta=expires_delta)
    save_token(token, description=description)
    return token


def save_token(encoded_token, description=None):
    """Add a new token to the database"""
    identity_claim = current_app.config["JWT_IDENTITY_CLAIM"]
    decoded_token = decode_token(encoded_token)
    jti = decoded_token["jti"]
    token_type = decoded_token["type"]
    user_id = int(decoded_token[identity_claim])
    iat = datetime.fromtimestamp(decoded_token["iat"])
    try:
        expires = datetime.fromtimestamp(decoded_token["exp"])
    except KeyError:
        expires = None
    db_token = models.Token(
        jti=jti,
        token_type=token_type,
        user_id=user_id,
        issued_at=iat,
        expires=expires,
        description=description,
    )
    db.session.add(db_token)
    db.session.commit()


def revoke_token(token_id, user_id):
    """Revoke the given token

    Raises a CSEntryError if the token does not exist in the database
    or if it doesn't belong to the given user
    """
    token = models.Token.query.get(token_id)
    if token is None:
        raise utils.CSEntryError(
            f"Could not find the token {token_id}", status_code=404
        )
    if token.user_id != user_id:
        raise utils.CSEntryError(
            f"Token {token_id} doesn't belong to user {user_id}", status_code=401
        )
    db.session.delete(token)
    db.session.commit()


def prune_database():
    """Delete tokens that have expired from the database"""
    current_app.logger.info("Delete expired tokens")
    now = datetime.now()
    expired = models.Token.query.filter(models.Token.expires < now).all()
    for token in expired:
        db.session.delete(token)
    db.session.commit()
