$(document).ready(function () {
  var attributes_table = $("#attributes_table").DataTable({
    ajax: function (data, callback, settings) {
      var kind = $("li a.nav-link.active").text();
      $.getJSON(
        $SCRIPT_ROOT + "/inventory/_retrieve_attributes/" + kind,
        function (json) {
          callback(json);
        }
      );
    },
    order: [[2, "asc"]],
    columnDefs: [
      {
        targets: [0],
        orderable: false,
        className: "text-center align-middle",
        render: function (data, type, row) {
          // render a checkbox to add/remove the attribute to the user's favorites
          var checked = data.favorite ? "checked" : "";
          return (
            '<input type="checkbox" value="' + data.id + '" ' + checked + ">"
          );
        },
        width: "5%",
      },
      {
        targets: [1],
        orderable: false,
        render: function (data, type, row) {
          // render QR code from base64 string
          return (
            '<img class="img-fluid" src="data:image/png;base64,' + data + '">'
          );
        },
        width: "10%",
      },
    ],
    paging: false,
  });

  // update the user favorites
  $("#attributes_table").on("change", 'input[type="checkbox"]', function () {
    var kind = $("li a.nav-link.active").text();
    $.ajax({
      type: "POST",
      url: $SCRIPT_ROOT + "/inventory/_update_favorites/" + kind,
      data: JSON.stringify({
        id: $(this).val(),
        checked: this.checked,
      }),
      contentType: "application/json",
    });
  });

  var attributes_favorites_table = $("#attributes_favorites_table").DataTable({
    ajax: function (data, callback, settings) {
      $.getJSON(
        $SCRIPT_ROOT + "/inventory/_retrieve_attributes_favorites",
        function (json) {
          callback(json);
        }
      );
    },
    order: [[1, "asc"]],
    columnDefs: [
      {
        targets: [0],
        orderable: false,
        render: function (data, type, row) {
          // render QR code from base64 string
          return (
            '<img class="img-fluid" src="data:image/png;base64,' + data + '">'
          );
        },
        width: "10%",
      },
    ],
    paging: false,
  });
});
