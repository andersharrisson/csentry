$(document).ready(function () {
  function group_type_update() {
    var hosts_selectize = $("#hosts")[0].selectize;
    if ($("#type option:selected").text() == "STATIC") {
      hosts_selectize.enable();
    } else {
      hosts_selectize.clear();
      hosts_selectize.disable();
    }
  }

  // Disable or enable hosts on group type update
  $("#type").on("change", function () {
    group_type_update();
  });

  // Disable or enable hosts on page load depending on group type
  if ($("#groupForm").length || $("#editGroupForm").length) {
    group_type_update();
  }

  if ($("#groupForm").length || $("#editGroupForm").length) {
    var groupVarsEditor = CodeMirror.fromTextArea(vars, {
      lineNumbers: true,
      mode: "yaml",
    });
    groupVarsEditor.setSize(null, 120);
    var handle = cmResize(groupVarsEditor, {
      minHeight: 120,
      resizableWidth: false,
      resizableHeight: true,
    });
  }

  var groups_table = $("#groups_table").DataTable({
    dom:
      "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-3 text-right'><'col-sm-12 col-md-3'f>>" +
      "<'row'<'col-sm-12'tr>>" +
      "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
    ajax: {
      url: $SCRIPT_ROOT + "/network/_retrieve_groups",
      type: "POST",
    },
    processing: true,
    serverSide: true,
    searchDelay: 500,
    stateSave: true,
    orderMulti: false,
    aaSorting: [],
    paging: true,
    pagingType: "full_numbers",
    columns: [
      {
        data: "name",
        render: function (data, type, row) {
          // render funtion to create link to group view page
          if (data === null) {
            return data;
          }
          var url = $SCRIPT_ROOT + "/network/groups/view/" + data;
          return '<a href="' + url + '">' + data + "</a>";
        },
      },
      {
        data: "vars",
        orderable: false,
        render: function (data, type, row) {
          if (data === null) {
            return "";
          }
          return (
            '<pre style="max-width:300px;">' +
            JSON.stringify(data, null, 2) +
            "</pre>"
          );
        },
      },
      { data: "type" },
      {
        data: "children",
        orderable: false,
        render: function (data, type, row) {
          return data.join(", ");
        },
      },
      {
        data: "hosts",
        orderable: false,
        render: function (data, type, row) {
          return data.join(", ");
        },
      },
    ],
  });

  if ($("#groups_table").length) {
    new $.fn.dataTable.Buttons(groups_table, {
      buttons: [
        {
          text: "Reset",
          className: "btn-outline-secondary",
          action: function (e, dt, node, conf) {
            dt.state.clear();
            dt.search("").order([]).draw();
          },
          init: function (api, node, config) {
            $(node).removeClass("btn-secondary");
          },
        },
      ],
    });

    groups_table
      .buttons()
      .container()
      .appendTo("#groups_table_wrapper .col-md-3:eq(0)");

    configure_search_tooltip();
  }
});
