$(document).ready(function () {
  function render_host_link(data) {
    // render funtion to create link to Host view page
    if (data === null) {
      return data;
    }
    var url = $SCRIPT_ROOT + "/network/hosts/view/" + data;
    return '<a href="' + url + '">' + data + "</a>";
  }

  function render_network_link(data) {
    // render funtion to create link to Network view page
    if (data === null) {
      return data;
    }
    var url = $SCRIPT_ROOT + "/network/networks/view/" + data;
    return '<a href="' + url + '">' + data + "</a>";
  }

  if ($("#hostForm").length || $("#editHostForm").length) {
    var hostVarsEditor = CodeMirror.fromTextArea(ansible_vars, {
      lineNumbers: true,
      mode: "yaml",
    });
    hostVarsEditor.setSize(null, 120);
    var handle = cmResize(hostVarsEditor, {
      minHeight: 120,
      resizableWidth: false,
      resizableHeight: true,
    });
  }

  function set_default_ip() {
    // Retrieve the first available IP for the selected network
    // and update the IP field
    var network_id = $("#network_id").val();
    if (network_id !== "") {
      $("#ip").removeClass("is-invalid");
      $.getJSON(
        $SCRIPT_ROOT + "/network/_retrieve_first_available_ip/" + network_id,
        function (json) {
          $("#ip").val(json.data);
        }
      );
    }
  }

  // And check / uncheck random_mac checkbox
  function update_device_type_attributes() {
    var device_type = $("#device_type_id option:selected").text();
    if (device_type.startsWith("Virtual")) {
      $("#random_mac").prop("checked", true).change();
    } else {
      $("#random_mac").prop("checked", false).change();
    }
  }

  // If random_mac is checked, generate a random address
  // Empty the field otherwise
  function fill_mac_address() {
    if ($("#random_mac").prop("checked")) {
      $.getJSON($SCRIPT_ROOT + "/network/_generate_random_mac", function (
        json
      ) {
        $("#mac").val(json.data.mac);
        $("#mac").prop("readonly", true);
      });
    } else {
      $("#mac").val("");
      $("#mac").prop("readonly", false);
    }
  }

  // Set the default IP on first page load for:
  // - register new host
  // - add interface
  // Do NOT replace the IP on edit interface page load!
  // (we have to keep the existing IP)
  // Do NOT replace the IP if the form was submitted and an error was raised
  // (we should display the same value that was submitted)
  if (
    ($("#hostForm").length || $("#interfaceForm").length) &&
    !$("input").hasClass("is-invalid")
  ) {
    set_default_ip();
  }

  // On page load if the network_id is invalid, it's because an empty entry was submitted
  // remove the default value to make it clear
  if (
    ($("#hostForm").length || $("#interfaceForm").length) &&
    $("#network_id").hasClass("is-invalid")
  ) {
    var network_id_selectize = $("#network_id")[0].selectize;
    network_id_selectize.clear(true);
  }

  // Clear the default value so that the user can start typing directly
  // or click a dropdown item
  $("#network_id")
    .next(".selectize-control")
    .on("click", function () {
      var network_id_selectize = $("#network_id")[0].selectize;
      network_id_selectize.clear(true);
    });

  // Set the default IP when changing network
  $("#network_id").on("change", function () {
    $(this).removeClass("is-invalid");
    $(this).next(".selectize-control").removeClass("is-invalid");
    set_default_ip();
  });

  // Only for register new host form
  if ($("#hostForm").length) {
    // On first page load
    update_device_type_attributes();

    // And change
    $("#device_type_id").on("change", function () {
      update_device_type_attributes();
    });
  }

  // Fill MAC address on page load
  if ($("#random_mac").length) {
    fill_mac_address();
  }

  // Fill or clear MAC address depending on random_mac checkbox
  $("#random_mac").on("change", function () {
    fill_mac_address();
  });

  var hosts_table = $("#hosts_table").DataTable({
    dom:
      "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-3 text-right'><'col-sm-12 col-md-3'f>>" +
      "<'row'<'col-sm-12'tr>>" +
      "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
    ajax: {
      url: $SCRIPT_ROOT + "/network/_retrieve_hosts",
      type: "POST",
    },
    processing: true,
    serverSide: true,
    searchDelay: 500,
    stateSave: true,
    orderMulti: false,
    aaSorting: [],
    pagingType: "full_numbers",
    pageLength: 20,
    lengthMenu: [
      [20, 50, 100],
      [20, 50, 100],
    ],
    columns: [
      {
        data: "name",
        render: function (data, type, row) {
          return render_host_link(data);
        },
      },
      { data: "device_type" },
      { data: "description", orderable: false },
      { data: "interfaces.0.ip", defaultContent: "", orderable: false },
      { data: "interfaces.0.mac", defaultContent: "", orderable: false },
      {
        data: "interfaces.0.network",
        render: function (data, type, row) {
          return render_network_link(data);
        },
        defaultContent: "",
        orderable: false,
      },
    ],
  });

  if ($("#hosts_table").length) {
    new $.fn.dataTable.Buttons(hosts_table, {
      buttons: [
        {
          text: "Reset",
          className: "btn-outline-secondary",
          action: function (e, dt, node, conf) {
            dt.state.clear();
            dt.search("").order([]).draw();
          },
          init: function (api, node, config) {
            $(node).removeClass("btn-secondary");
          },
        },
      ],
    });

    hosts_table
      .buttons()
      .container()
      .appendTo("#hosts_table_wrapper .col-md-3:eq(0)");

    configure_search_tooltip();
  }
});
