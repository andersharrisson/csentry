$(document).ready(function () {
  $.fn.focusWithoutScrolling = function () {
    var x = window.scrollX,
      y = window.scrollY;
    this.focus();
    window.scrollTo(x, y);
    return this; //chainability
  };

  // Focus to ICS id when loading the page
  $("#ics_id").focus();

  // Populate the stack member field linked to the host on first page load
  update_stack_member();

  // Prevent enter key to submit the form when scanning a label
  // and remove the ICS:ics_id: prefix
  $("#ics_id").keydown(function (event) {
    if (event.keyCode == 13) {
      event.preventDefault();
      var value = $(this).val();
      $(this).val(value.replace("CSE:ics_id:", ""));
      $("#serial_number").focus();
      return false;
    }
  });

  // Prevent enter key to submit the form when scanning serial number
  // Focus on the submit button so that scanning the Submit action
  // will submit the form (due to the CR sent by the scanner)
  $("#serial_number").keydown(function (event) {
    if (event.keyCode == 13) {
      event.preventDefault();
      $("#submit").focusWithoutScrolling();
      return false;
    }
  });

  $("#clear").click(function () {
    // clear all select fields
    $("select").val("");
  });

  // Update the stack member field linked to the host when changing it
  $("#host_id").on("change", function () {
    update_stack_member();
  });
});
