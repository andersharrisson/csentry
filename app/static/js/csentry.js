function flash_alert(message, category) {
  var htmlString =
    '<div class="alert alert-' + category + ' alert-dismissible" role="alert">';
  htmlString +=
    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
  htmlString +=
    '<span aria-hidden="true">&times;</span></button>' + message + "</div>";
  $(htmlString).prependTo("#mainContent").hide().slideDown();
}

function remove_alerts() {
  $(".alert").slideUp("normal", function () {
    $(this).remove();
  });
}

function update_progress_bar(value) {
  // Assume that there is only one progress bar
  $(".progress-bar")
    .css("width", value + "%")
    .attr("aria-valuenow", value)
    .text(value + "%");
}

// Send a request to the server every 500 ms
// until the job is done
function check_job_status(status_url, $modal) {
  // cache shall be set to false for this to work in IE!
  $.ajax({
    dataType: "json",
    url: status_url,
    cache: false,
    success: function (data, status, request) {
      switch (data.status) {
        case "unknown":
          $modal.modal("hide");
          flash_alert("Unknown job id", "danger");
          break;
        case "finished":
          $modal.modal("hide");
          switch (data.func_name) {
            case "generate_items_excel_file":
              window.location.href =
                $SCRIPT_ROOT + "/static/files/" + data.result;
              break;
          }
          break;
        case "failed":
          $modal.modal("hide");
          flash_alert(data.message, "danger");
          break;
        case "started":
          if (data.progress !== null) {
            update_progress_bar(data.progress);
          }
        default:
          // queued/started/deferred
          setTimeout(function () {
            check_job_status(status_url, $modal);
          }, 500);
      }
    },
  });
}

// Function to dynamically update a select field
function update_selectfield(field_id, data, selected_value) {
  var $field = $(field_id);
  $field.empty();
  $.map(data, function (option, index) {
    if (option == "None") {
      var text = "";
    } else {
      var text = option;
    }
    $field.append($("<option></option>").attr("value", option).text(text));
  });
  if (selected_value == "") {
    $field.prop("disabled", true);
  } else {
    $field.val(selected_value);
    $field.prop("disabled", false);
  }
}

// Function to populate dynamically the stack_member field
// in the create and edit item forms
function update_stack_member() {
  // Retrieve free stack members
  var host_id = $("#host_id").val();
  $.getJSON(
    $SCRIPT_ROOT + "/inventory/_retrieve_free_stack_members/" + host_id,
    {
      ics_id: $("#ics_id").val(),
    },
    function (json) {
      update_selectfield(
        "#stack_member",
        json.data.stack_members,
        json.data.selected_member
      );
      $("#stack_member").prop("disabled", json.data.disabled);
    }
  );
}

// Function to initialize tooltip on DataTables search box
// Only used when using server side processing
function configure_search_tooltip() {
  $(".dataTables_filter").tooltip({
    title:
      "Enter keyword(s) or use wildcard character '*' for partial match. See the help for more information.",
  });

  // Hide the tooltip when clicking in the search box
  $("input[type='search'").focus(function () {
    $(".dataTables_filter").tooltip("hide");
  });
}

$(document).ready(function () {
  // When an invalid input was submitted, the server
  // adds the "is-invalid" class to form fields to display
  // them in red with an error message
  // When starting to type again, we want to remove the error
  // message to not confuse the user
  $('input[type="text"]').keyup(function (event) {
    $(this).removeClass("is-invalid");
  });

  // Initialize selectize with the default options
  // for all Select and MultiSelect fields with the
  // selectize-default class
  $(".selectize-default").selectize();
});
