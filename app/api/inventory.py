# -*- coding: utf-8 -*-
"""
app.api.inventory
~~~~~~~~~~~~~~~~~

This module implements the inventory API.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
from flask import Blueprint, jsonify, request, current_app
from flask_login import login_required
from .. import utils, models
from ..decorators import login_groups_accepted
from .utils import commit, create_generic_model, get_generic_model, delete_generic_model

bp = Blueprint("inventory_api", __name__)


def get_item_by_id_or_ics_id(id_):
    """Retrieve item by id or ICS id"""
    try:
        item_id = int(id_)
    except ValueError:
        # Assume id_ is an ics_id
        item = models.Item.query.filter_by(ics_id=id_).first()
    else:
        item = models.Item.query.get(item_id)
    if item is None:
        raise utils.CSEntryError(f"Item id '{id_}' not found", status_code=404)
    return item


@bp.route("/items")
@login_required
def get_items():
    """Return items

    .. :quickref: Inventory; Get items
    """
    return get_generic_model(models.Item, order_by=models.Item.created_at)


@bp.route("/items/<id_>")
@login_required
def get_item(id_):
    r"""Retrieve item by id or ICS id

    .. :quickref: Inventory; Get item by id or ICS id

    :param id\_: ICS id or primary key of the item
    """
    item = get_item_by_id_or_ics_id(id_)
    return jsonify(item.to_dict())


@bp.route("/items", methods=["POST"])
@login_groups_accepted("admin", "inventory")
def create_item():
    """Register a new item

    .. :quickref: Inventory; Create new item

    :jsonparam serial_number: serial number
    :jsonparam ics_id: (optional) ICS id (temporary one generated if not present)
    :jsonparam quantity: (optional) number of items [default: 1]
    :jsonparam manufacturer: (optional) name of the manufacturer
    :jsonparam model: (optional) name of the model
    :jsonparam location: (optional) name of the location
    :jsonparam status: (optional) name of the status
    :jsonparam parent_id: (optional) parent id
    :jsonparam host_id: (optional) host id
    """
    # People should assign an ICS id to a serial number when creating
    # an item so ics_id should also be a mandatory field.
    # But there are existing items (in confluence and JIRA) that we want to
    # import and associate after they have been created.
    # In that case a temporary id is automatically assigned.
    return create_generic_model(models.Item, mandatory_fields=("serial_number",))


@bp.route("/items/<id_>", methods=["PATCH"])
@login_groups_accepted("admin", "inventory")
def patch_item(id_):
    r"""Patch an existing item

    .. :quickref: Inventory; Update existing item

    :param id\_: ICS id or primary key of the item
    :jsonparam ics_id: ICS id - only allowed if the current one is temporary
    :jsonparam manufacturer: Item's manufacturer
    :jsonparam model: Item's model
    :jsonparam location: Item's location
    :jsonparam status: Item's status
    :jsonparam parent: Item's parent
    """
    data = request.get_json()
    if data is None:
        raise utils.CSEntryError("Body should be a JSON object")
    if not data:
        raise utils.CSEntryError("At least one field is required", status_code=422)
    for key in data.keys():
        if key not in (
            "ics_id",
            "manufacturer",
            "model",
            "location",
            "status",
            "parent",
        ):
            raise utils.CSEntryError(f"Invalid field '{key}'", status_code=422)
    item = get_item_by_id_or_ics_id(id_)
    # Only allow to set ICS id if the current id is a temporary one
    if item.ics_id.startswith(current_app.config["TEMPORARY_ICS_ID"]):
        item.ics_id = data.get("ics_id", item.ics_id)
    elif "ics_id" in data:
        raise utils.CSEntryError("'ics_id' can't be changed", status_code=422)
    item.manufacturer = utils.convert_to_model(
        data.get("manufacturer", item.manufacturer), models.Manufacturer
    )
    item.model = utils.convert_to_model(data.get("model", item.model), models.Model)
    item.location = utils.convert_to_model(
        data.get("location", item.location), models.Location
    )
    item.status = utils.convert_to_model(data.get("status", item.status), models.Status)
    parent_ics_id = data.get("parent")
    if parent_ics_id is not None:
        parent = models.Item.query.filter_by(ics_id=parent_ics_id).first()
        if parent is not None:
            item.parent_id = parent.id
            # Update location and status with those from parent
            item.location = parent.location
            item.status = parent.status
    # Update all children status and location
    for child in item.children:
        child.location = item.location
        child.status = item.status
    commit()
    return jsonify(item.to_dict())


@bp.route("/items/<int:item_id>", methods=["DELETE"])
@login_groups_accepted("admin")
def delete_item(item_id):
    """Delete an item

    .. :quickref: Inventory; Delete an item

    :param item_id: item primary key
    """
    return delete_generic_model(models.Item, item_id)


@bp.route("/items/<id_>/comments")
@login_required
def get_item_comments(id_):
    r"""Get item comments

    .. :quickref: Inventory; Get item comments

    :param id\_: ICS id or primary key of the item
    """
    item = get_item_by_id_or_ics_id(id_)
    return jsonify([comment.to_dict() for comment in item.comments])


@bp.route("/items/<id_>/comments", methods=["POST"])
@login_groups_accepted("admin", "inventory")
def create_item_comment(id_):
    r"""Create a comment on item

    .. :quickref: Inventory; Create comment on item

    :param id\_: ICS id or primary key of the item
    :jsonparam body: comment body
    """
    item = get_item_by_id_or_ics_id(id_)
    return create_generic_model(
        models.ItemComment, mandatory_fields=("body",), item_id=item.id
    )


@bp.route("/items/comments/<int:comment_id>", methods=["DELETE"])
@login_groups_accepted("admin")
def delete_comment(comment_id):
    """Delete an item comment

    .. :quickref: Inventory; Delete an item comment

    :param comment_id: comment primary key
    """
    return delete_generic_model(models.ItemComment, comment_id)


@bp.route("/actions")
@login_required
def get_actions():
    """Get actions

    .. :quickref: Inventory; Get actions
    """
    return get_generic_model(models.Action)


@bp.route("/manufacturers")
@login_required
def get_manufacturers():
    """Get manufacturers

    .. :quickref: Inventory; Get manufacturers
    """
    return get_generic_model(models.Manufacturer)


@bp.route("/manufacturers", methods=["POST"])
@login_groups_accepted("admin", "inventory")
def create_manufacturer():
    """Create a new manufacturer

    .. :quickref: Inventory; Create new manufacturer

    :jsonparam name: manufacturer name
    :jsonparam description: (optional) description
    """
    return create_generic_model(models.Manufacturer)


@bp.route("/manufacturers/<int:manufacturer_id>", methods=["DELETE"])
@login_groups_accepted("admin")
def delete_manufacturer(manufacturer_id):
    """Delete a manufacturer

    .. :quickref: Inventory; Delete a manufacturer

    :param manufacturer_id: manufacturer primary key
    """
    return delete_generic_model(models.Manufacturer, manufacturer_id)


@bp.route("/models")
@login_required
def get_models():
    """Get models

    .. :quickref: Inventory; Get models
    """
    return get_generic_model(models.Model)


@bp.route("/models", methods=["POST"])
@login_groups_accepted("admin", "inventory")
def create_model():
    """Create a new model

    .. :quickref: Inventory; Create new model

    :jsonparam name: model name
    :jsonparam description: (optional) description
    """
    return create_generic_model(models.Model)


@bp.route("/models/<int:model_id>", methods=["DELETE"])
@login_groups_accepted("admin")
def delete_model(model_id):
    """Delete a model

    .. :quickref: Inventory; Delete a model

    :param model_id: model primary key
    """
    return delete_generic_model(models.Model, model_id)


@bp.route("/locations")
@login_required
def get_locations():
    """Get locations

    .. :quickref: Inventory; Get locations
    """
    return get_generic_model(models.Location)


@bp.route("/locations", methods=["POST"])
@login_groups_accepted("admin", "inventory")
def create_locations():
    """Create a new location

    .. :quickref: Inventory; Create new location

    :jsonparam name: location name
    :jsonparam description: (optional) description
    """
    return create_generic_model(models.Location)


@bp.route("/locations/<int:location_id>", methods=["DELETE"])
@login_groups_accepted("admin")
def delete_location(location_id):
    """Delete a location

    .. :quickref: Inventory; Delete a location

    :param location_id: location primary key
    """
    return delete_generic_model(models.Location, location_id)


@bp.route("/statuses")
@login_required
def get_status():
    """Get statuses

    .. :quickref: Inventory; Get statuses
    """
    return get_generic_model(models.Status)


@bp.route("/statuses", methods=["POST"])
@login_groups_accepted("admin", "inventory")
def create_status():
    """Create a new status

    .. :quickref: Inventory; Create new status

    :jsonparam name: status name
    :jsonparam description: (optional) description
    """
    return create_generic_model(models.Status)


@bp.route("/statuses/<int:status_id>", methods=["DELETE"])
@login_groups_accepted("admin")
def delete_status(status_id):
    """Delete a status

    .. :quickref: Inventory; Delete a status

    :param status_id: status primary key
    """
    return delete_generic_model(models.Status, status_id)


@bp.route("/macs")
@login_required
def get_macs():
    """Return mac addresses

    .. :quickref: Inventory; Get mac addresses
    """
    return get_generic_model(models.Mac, order_by=models.Mac.address)


@bp.route("/macs", methods=["POST"])
@login_groups_accepted("admin", "inventory")
def create_macs():
    """Create a new mac address

    .. :quickref: Inventory; Create new mac address

    :jsonparam address: MAC address
    :jsonparam item_id: (optional) linked item primary key
    """
    return create_generic_model(models.Mac, mandatory_fields=("address",))
