# -*- coding: utf-8 -*-
"""
app.task.views
~~~~~~~~~~~~~~

This module implements the task blueprint.

:copyright: (c) 2018 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
from flask import Blueprint, render_template, jsonify, request
from flask_login import login_required, current_user
from .. import models

bp = Blueprint("task", __name__)


@bp.route("/tasks")
@login_required
def list_tasks():
    return render_template("task/tasks.html")


@bp.route("/tasks/view/<id_>")
@login_required
def view_task(id_):
    task = models.Task.query.get_or_404(id_)
    return render_template("task/view_task.html", task=task)


@bp.route("/_retrieve_tasks")
@login_required
def retrieve_tasks():
    all = request.args.get("all", "false") == "true"
    data = [task.to_dict() for task in current_user.get_tasks(all=all)]
    return jsonify(data=data)
