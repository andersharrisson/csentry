Changelog
=========

Version 2021.04.06
------------------

- Validate group names (INFRA-3135)
- Add warning text for creating vm (INFRA-3292)
- Paginate history in ansible groups and hosts (INFRA-3136)

Version 2020.11.25
------------------

- Add documentation about database versioning
- Set display_name to username when empty (INFRA-2909)
- Improve logging (INFRA-2899)

Version 2020.11.13
------------------

- Increased hostname character limit from 20 -> 24 (INFRA-2808)
- Use SSL by default for LDAP connection (INFRA-2805)
- Use SonarScanner.gitlab-ci.yml template (INFRA-2832)

Version 2020.10.30
------------------

- Fix inventory export to excel (INFRA-2780)
- Add developer documentation

Version 2020.10.27
------------------

- Update Python and requirements (INFRA-2649)
- Update RQ and rq-dashboard (INFRA-2649 / INFRA-2754)
- Fix code smells (INFRA-2729)
- Add auditor group (INFRA-2728)
- Update Sphinx and switch to sphinx_rtd_theme (INFRA-2742)

Version 2020.10.06
------------------

- Make sensitive hosts visible for members of the scope (INFRA-2508)
- Only hide sensitive networks not part of the user scope (INFRA-2437)
- Automatically set network_scope group as parent of network groups (INFRA-2639)

Version 2020.07.08
------------------

- Add view to edit network scope (INFRA-2316)
- Add API endpoint to patch network scope (INFRA-2316)

Version 2020.06.12
------------------

- Change MTCA-IOxOS to MTCA-IFC (INFRA-2200)
- Add description field on Interface (INFRA-2112)

Version 2020.04.16
------------------

- Allow /network/networks API endpoint for normal users (INFRA-2013)
- Update Bootstrap to 4.4.1 (INFRA-2021)
- Update DataTables (INFRA-2021)
- Add tooltip on search box (INFRA-1893)

Version 2020.03.17
------------------

- Add extra badges to README and documentation
- Fix get_hosts for normal users (INFRA-1888)

Version 2020.03.09
------------------

- Trigger core services update on host change (INFRA-1846)
- Add DELETE method on all API endpoints (INFRA-1853)
- Remove create_user API endpoint (INFRA-1854)

Version 2020.03.05
------------------

- Allow users to view networks (INFRA-1809)
- Add HOSTNAME Ansible group type (INFRA-1844)
- Remove IOC git repository creation (INFRA-1839)
- Make IP optional when creating interface via the API (INFRA-1833)
- Introduce new device types for MTCA (INFRA-1835)


Version 2020.02.25
------------------

- Fix inventory update (INFRA-1792)

Version 2020.02.24
------------------

- Add app version in navbar (INFRA-1789)
- Allow to pass LDAP settings as env variables (INFRA-1788)
- Implement server side processing for Ansible groups (INFRA-1790)

Version 0.29.0 (2020-02-03)
---------------------------

- Add MAC address column to hosts table (INFRA-1669)
- Restrict view of networks and scopes to admin only (INFRA-1671)
- Add sensitive field to Network class (INFRA-1671)
- Filter out sensitive hosts for non admin users (INFRA-1671)
- Add API endpoint and view to update network (INFRA-1724)
- Make pages title more explicit (INFRA-1726)

Version 0.28.0 (2019-12-19)
---------------------------

- Fix exception when setting new stack_member (INFRA-1648)
- Prevent recursive dependency loop in Ansible groups (INFRA-1622)
- Prevent network and scope overlapping (INFRA-1627)
- Add netmask to network view (INFRA-1660)

Version 0.27.1 (2019-12-04)
---------------------------

- Fix regexp for ICS id validation (INFRA-1569)
- Allow normal users to edit hosts with no interface (INFRA-1604)

Version 0.27.0 (2019-10-30)
---------------------------

- Increase interface name max length (INFRA-1324)
- Add endpoints to patch host and interface (INFRA-1406)
- Limit networks to same scope for extra interfaces (INFRA-1297)

Version 0.26.1 (2019-09-18)
---------------------------

- Fix inventory update not triggered (INFRA-1290)

Version 0.26.0 (2019-09-18)
---------------------------

- Decouple inventory and core services update (INFRA-1290)
- Add broadcast address to network view (INFRA-1291)
- Add view network scope page (INFRA-1292)
- Make ansible_vars in host searchable (INFRA-1295)
- Remove Generate ZTP configuration function (INFRA-1299)

Version 0.25.1 (2019-08-30)
---------------------------

- Prevent input of invalid Ansible variables (INFRA-1221)
- Update black and pre-commit config
- Add pytest.ini to speed test discovery

Version 0.25.0 (2019-08-22)
---------------------------

- Implement user access on network scope instead of domain (INFRA-1228)
- Create IOC repositories with same path as name (INFRA-1230)

Version 0.24.1 (2019-07-10)
---------------------------

- Fix sorting of items with some null stack_member (INFRA-1112)

Version 0.24.0 (2019-07-10)
---------------------------

- Add serial_number and stack_member to hosts json model (INFRA-1111)

Version 0.23.0 (2019-06-14)
---------------------------

- Increase RQ default timeout (INFRA-1051)

Version 0.22.0 (2019-06-14)
---------------------------

- Make vlan optional in NetworkScope and Network (INFRA-1049)
- Update reverse dependencies of failed tasks (INFRA-1051)

Version 0.21.1 (2019-05-13)
---------------------------

- Fix users synchronization (INFRA-1026)
- Remove caching of user retrieval (INFRA-1025)

Version 0.21.0 (2019-05-10)
---------------------------

- Improve documentation about ansible-vault
- Fix IOC host indexation (INFRA-1015)
- Allow users to delete their host (INFRA-1018)
- Fix job.status DeprecationWarning (INFRA-1021)
- Allow service users to login (INFRA-1022)

Version 0.20.2 (2019-05-08)
---------------------------

- Fix IOC repository creation (INFRA-1015)
- Save ansible var when setting boot profile (INFRA-1016)

Version 0.20.1 (2019-04-25)
---------------------------

- Save the device_type_id in the session (INFRA-987)
- Fix exception when no network selected (INFRA-810)
- Remove graylog support in uwsgi (INFRA-981)
- Replace raven with sentry-sdk (INFRA-979)

Version 0.20.0 (2019-04-05)
---------------------------

- Add API endpoint to search hosts (INFRA-931)
- Initialize IOC repository on GitLab (INFRA-932)
- Limit the max number of elements returned per page (INFRA-942)
- Allow to set the boot profile from CSEntry (INFRA-943)

Version 0.19.2 (2019-03-27)
---------------------------

- Fix model update in admin interface (INFRA-908)
- Increase timeout for core services update (INFRA-895)
- Add depends_on field on Task (INFRA-895)

Version 0.19.1 (2019-03-18)
---------------------------

- Fix excel file download (INFRA-890)
- Skip post install for windows VM creation (INFRA-877)

Version 0.19.0 (2019-03-18)
---------------------------

- Add osversion field to CreateVMForm (INFRA-877)
- Fix link to AWX job id for workflow jobs (INFRA-886)
- Allow users to create VM/VIOC (INFRA-775)
- Trigger AWX inventory update on database change (INFRA-887)

Version 0.18.2 (2019-03-07)
---------------------------

- Fix post install job trigger (INFRA-870)

Version 0.18.1 (2019-03-07)
---------------------------

- Clean extra vars passed for VM and VIOC creation (INFRA-870)

Version 0.18.0 (2019-03-05)
---------------------------

- Add gateway field to network table (INFRA-809)
- Add view network page (INFRA-860)
- Remove tags and add is_ioc field to host table (INFRA-862)
- Allow to launch a workflow job (INFRA-867)

Version 0.17.1 (2019-02-07)
---------------------------

- Catch exception raised by Unique Validator (INFRA-777)
- Only check the main interface in dynamic groups (INFRA-784)
- Make sure the first interface is the main one (INFRA-785)

Version 0.17.0 (2019-01-28)
---------------------------

- Implement new layout (INFRA-763)
- Fix case insensitive search (INFRA-770)
- Add post install job after VM creation (INFRA-769)

Version 0.16.0 (2019-01-23)
---------------------------

- Explicitely define the elasticsearch mapping (INFRA-723)
- Add favicon (INFRA-725)
- Pass disk size in inventory when creating VM (INFRA-759)

Version 0.15.1 (2018-12-10)
---------------------------

- Fix extra interface creation (INFRA-697)

Version 0.15.0 (2018-11-30)
---------------------------

- Disable DHCP & DNS update on host modification (INFRA-673)
- Use different groups for inventory and network (INFRA-578)
- Add network permissions per domain (INFRA-578)
- Move mac API to inventory endpoint (INFRA-674)
- Add button to delete Ansible group (INFRA-678)
- Save VM memory and cores as ansible variables (INFRA-640)
- Return host fqdn in Ansible groups (INFRA-640)
- Add extra fields to the API (INFRA-640)
- Update Python to 3.7.1 and dependencies (INFRA-684)
- Fix WhiteNoise used static directory (INFRA-683)
- Fix flask subcommand name (INFRA-687)

Version 0.14.0 (2018-11-08)
---------------------------

- Fix TypeError in after_commit (INFRA-613)
- Add zabbix plugin to uwsgi (INFRA-614)
- Allow to edit and delete comments (INFRA-618)
- Make mac addresses unique by interface (INFRA-639)
- Replace bootstrap-select with selectize (INFRA-644)
- Make host/interface/cname unique (INFRA-245)
- Switch to new template to deploy VM in proxmox (INFRA-651)

Version 0.13.0 (2018-10-12)
---------------------------

- Implement full text search for items (INFRA-575)
- Implement server side processing and full text search for network hosts (INFRA-595)
- Enable datatables state saving (INFRA-608)
- Compile uwsgi with graylog2 plugin (INFRA-576)
- Fix javascript error: Cannot read property '_buttons' of undefined (INFRA-584)
- Fix LDAPInvalidFilterErrorldap3 (INFRA-550)
- Fix IndexError when mail is set to an empty list (INFRA-610)

Version 0.12.0 (2018-09-28)
---------------------------

- Update DataTables with bootstrap 4 theme (INFRA-545)
- Implement alert flashing from javascript (INFRA-545)
- Add button to export items to excel file (INFRA-545)
- Add button to delete hosts (INFRA-557)
- Allow to delete hosts and interfaces via the API (INFRA-570)
- Display hosts with no interface on list hosts page (INFRA-569)
- Ignore old "lost" deferred tasks (INFRA-559)

Version 0.11.4 (2018-09-24)
---------------------------

- Replace AWX username/password with oauth token for deployment
- Allow to resize the CodeMirror editor (INFRA-543)
- Add yaml constructor to support "!vault" tag (INFRA-544)
- Fix TowerCLIError when ztp_stack is empty (INFRA-547)
- Fix AttributeError when creating host with group (INFRA-548)
- Enable sentry user feedback for crash reports

Version 0.11.3 (2018-09-14)
---------------------------

- Add sentry integration (INFRA-525)
- Pass sensitive information via environment variables

Version 0.11.2 (2018-09-13)
---------------------------

- Add versioning on Host and AnsibleGroup (INFRA-500)
- Disable artifact downloads in release and deploy stages

Version 0.11.1 (2018-08-21)
---------------------------

- Update tower-cli version for deployment
- Add host FQDN in host view (INFRA-459)
- Pass ztp_stack var to trigger_ztp_configuration (INFRA-458)
- Update Ansible groups documentation

Version 0.11.0 (2018-08-16)
---------------------------

- Add Ansible parent - child groups
- Add Ansible dynamic groups
- Remove flask-bootstrap dependency
- Update to bootstrap 4.1.3
- Switch to python-slim image and update requirements
- Replace jwt decorators with flask-login

Version 0.10.2 (2018-07-23)
---------------------------

- Allow retrieving interfaces by network name (INFRA-424)

Version 0.10.1 (2018-07-17)
---------------------------

- Wrap long lines in Ansible groups table
- Add model field to /networks/hosts endpoint (INFRA-414)
- Add model field to /networks/interfaces endpoint (INFRA-414)
- Add trigger ZTP configuration task (INFRA-414)

Version 0.10.0 (2018-07-16)
---------------------------

- Use sqlalchemy events hook to trigger tasks
- Add Ansible variables on the host table (INFRA-412)
- Add Ansible groups table and views (INFRA-412)
- Add new api endpoint /network/groups (INFRA-412)


Version 0.9.0 (2018-07-06)
--------------------------

- Redirect to the view host page when creating a host (INFRA-402)
- Save tasks (background jobs) to the database (INFRA-403)
- Add blueprint to view tasks (INFRA-403)
- Redirect to the task view page when creating a VM
- Display host creation date and user on view host page
- Use black for source code formatting
- Add pre-commit hooks for code formatting and linting
- Update documentation

Version 0.8.1
-------------

- Fix VM creation (memory shall be passed in MB)

Version 0.8.0
-------------

- Hide interface name in create host form (INFRA-287)
- Allow to link several items to one host (INFRA-267)
- Add stack_member field to item (INFRA-267)
- Add extra device types (INFRA-302)
- Add IOC tag (INFRA-302)
- Select the IOC tag by default based on device type (INFRA-302)
- Use bootstrap-select for tags selection
- Use the last IP as network gateway (INFRA-339)
- Fix default selected tags in edit interface view (INFRA-344)
- Use CamelCase for CSEntry device types and tags (INFRA-334)
- Add RQ to process jobs in the background
- Add RQ Dashboard blueprint (admin only)
- Automatically trigger update of TN core services on host or interface change
- Add Create VM button (admin only for now)
- Allow to easily identify staging server (INFRA-347)
- Update documentation

Version 0.7.0
-------------

- Add search in model description from inventory page
- Add model description to view item (INFRA-280)
- Add device_type table (INFRA-281)
- Add device_type to interfaces API endpoint (INFRA-277)
- Add attributes favorites page (INFRA-283)
- Sort network names in the register new host form (INFRA-284)
- Clear error messages on form fields on change (INFRA-285)

Version 0.6.8
-------------

- return username instead of display name in the API (INFRA-241)
- add /network/cnames API endpoint
- update default available IPs range

Version 0.6.7
-------------

- accept a list of LDAP groups

Version 0.6.6
-------------

- add link to documentation
- add Action tab next to Attributes
- switch to centos-miniconda3 base image

Version 0.6.5
-------------

- add /network/domains API endpoint
- add interfaces filtering by domain
- add netmask field in network json representation
- increase QRCode size

Version 0.6.4
-------------

- add API endpoint to retrieve the current user profile
- add Submit action QRCode to create an item
- update scanner instructions
- update documentation

Version 0.6.3
-------------

- add CHANGELOG
- add sphinx dependency to build documentation
- add stage to deploy documentation to GitLab pages
- move QR codes to attributes table
- improve documentation
- add manual job to deploy to production
