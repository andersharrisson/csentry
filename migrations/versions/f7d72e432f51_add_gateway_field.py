"""Add gateway field

Revision ID: f7d72e432f51
Revises: 7c38e78b6de6
Create Date: 2019-02-27 17:35:22.535126

"""
import ipaddress
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "f7d72e432f51"
down_revision = "7c38e78b6de6"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("network", sa.Column("gateway", postgresql.INET(), nullable=True))
    network = sa.sql.table(
        "network",
        sa.sql.column("id"),
        sa.sql.column("address"),
        sa.sql.column("gateway"),
    )
    # Fill the gateway based on the network address
    conn = op.get_bind()
    res = conn.execute("SELECT id, address FROM network")
    results = res.fetchall()
    for result in results:
        address = ipaddress.ip_network(result[1])
        hosts = list(address.hosts())
        # Use last IP by default
        gateway = str(hosts[-1])
        op.execute(
            network.update().where(network.c.id == result[0]).values(gateway=gateway)
        )
    op.create_check_constraint(
        op.f("ck_network_gateway_in_network"), "network", "gateway << address"
    )
    op.create_unique_constraint(op.f("uq_network_gateway"), "network", ["gateway"])
    op.alter_column("network", "gateway", nullable=False)


def downgrade():
    op.drop_constraint(op.f("uq_network_gateway"), "network", type_="unique")
    op.drop_constraint(op.f("ck_network_gateway_in_network"), "network", type_="check")
    op.drop_column("network", "gateway")
