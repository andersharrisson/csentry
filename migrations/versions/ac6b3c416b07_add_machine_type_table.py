"""Add machine_type table

Revision ID: ac6b3c416b07
Revises: dfd4eae61224
Create Date: 2018-04-06 12:17:16.469046

"""
from alembic import op
import sqlalchemy as sa
import citext


# revision identifiers, used by Alembic.
revision = "ac6b3c416b07"
down_revision = "dfd4eae61224"
branch_labels = None
depends_on = None


def upgrade():
    machine_type = op.create_table(
        "machine_type",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", citext.CIText(), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_machine_type")),
        sa.UniqueConstraint("name", name=op.f("uq_machine_type_name")),
    )
    # WARNING! If the database is not empty, we can't set the machine_type_id to nullable=False before adding a value!
    op.add_column("host", sa.Column("machine_type_id", sa.Integer(), nullable=True))
    op.create_foreign_key(
        op.f("fk_host_machine_type_id_machine_type"),
        "host",
        "machine_type",
        ["machine_type_id"],
        ["id"],
    )
    # Create the Physical and Virtual machine types
    op.execute(
        machine_type.insert().values(
            [{"id": 1, "name": "Physical"}, {"id": 2, "name": "Virtual"}]
        )
    )
    # Fill the host machine_type_id based on the value from the type column
    host = sa.sql.table("host", sa.sql.column("machine_type_id"), sa.sql.column("type"))
    op.execute(host.update().where(host.c.type == "Physical").values(machine_type_id=1))
    op.execute(host.update().where(host.c.type == "Virtual").values(machine_type_id=2))
    op.drop_column("host", "type")
    # Add the nullable=False constraint
    op.alter_column("host", "machine_type_id", nullable=False)


def downgrade():
    op.add_column(
        "host", sa.Column("type", sa.TEXT(), autoincrement=False, nullable=True)
    )
    host = sa.sql.table("host", sa.sql.column("machine_type_id"), sa.sql.column("type"))
    op.execute(host.update().where(host.c.machine_type_id == 1).values(type="Physical"))
    op.execute(host.update().where(host.c.machine_type_id == 2).values(type="Virtual"))
    op.drop_constraint(
        op.f("fk_host_machine_type_id_machine_type"), "host", type_="foreignkey"
    )
    op.drop_column("host", "machine_type_id")
    op.drop_table("machine_type")
