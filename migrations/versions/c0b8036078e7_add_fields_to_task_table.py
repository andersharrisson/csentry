"""add fields to task table

Revision ID: c0b8036078e7
Revises: 7d0d580cdb1a
Create Date: 2018-07-02 11:45:28.255006

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "c0b8036078e7"
down_revision = "7d0d580cdb1a"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("task", sa.Column("awx_job_id", sa.Integer(), nullable=True))
    op.add_column("task", sa.Column("ended_at", sa.DateTime(), nullable=True))


def downgrade():
    op.drop_column("task", "ended_at")
    op.drop_column("task", "awx_job_id")
