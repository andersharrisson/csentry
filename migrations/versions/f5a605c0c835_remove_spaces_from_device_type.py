"""remove spaces from device_type

Revision ID: f5a605c0c835
Revises: ea606be23b95
Create Date: 2018-05-22 13:41:28.137611

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "f5a605c0c835"
down_revision = "ea606be23b95"
branch_labels = None
depends_on = None


def upgrade():
    device_type = sa.sql.table(
        "device_type", sa.sql.column("id"), sa.sql.column("name")
    )
    op.execute(
        device_type.update()
        .where(device_type.c.name == "Physical Machine")
        .values(name="PhysicalMachine")
    )
    op.execute(
        device_type.update()
        .where(device_type.c.name == "Virtual Machine")
        .values(name="VirtualMachine")
    )


def downgrade():
    device_type = sa.sql.table(
        "device_type", sa.sql.column("id"), sa.sql.column("name")
    )
    op.execute(
        device_type.update()
        .where(device_type.c.name == "PhysicalMachine")
        .values(name="Physical Machine")
    )
    op.execute(
        device_type.update()
        .where(device_type.c.name == "VirtualMachine")
        .values(name="Virtual Machine")
    )
