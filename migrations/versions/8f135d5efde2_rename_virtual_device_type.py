"""Rename Virtual device type

Revision ID: 8f135d5efde2
Revises: 573560351033
Create Date: 2018-04-23 07:43:43.195703

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "8f135d5efde2"
down_revision = "573560351033"
branch_labels = None
depends_on = None


def upgrade():
    device_type = sa.sql.table(
        "device_type", sa.sql.column("id"), sa.sql.column("name")
    )
    op.execute(
        device_type.update()
        .where(device_type.c.name == "Virtual")
        .values(name="Virtual Machine")
    )


def downgrade():
    device_type = sa.sql.table(
        "device_type", sa.sql.column("id"), sa.sql.column("name")
    )
    op.execute(
        device_type.update()
        .where(device_type.c.name == "Virtual Machine")
        .values(name="Virtual")
    )
