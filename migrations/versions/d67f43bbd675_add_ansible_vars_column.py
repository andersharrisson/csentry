"""Add ansible_vars column

Revision ID: d67f43bbd675
Revises: a9442567c6dc
Create Date: 2018-07-10 11:39:17.955468

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "d67f43bbd675"
down_revision = "a9442567c6dc"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "host",
        sa.Column(
            "ansible_vars", postgresql.JSONB(astext_type=sa.Text()), nullable=True
        ),
    )


def downgrade():
    op.drop_column("host", "ansible_vars")
