"""Add depends_on field on Task

Revision ID: cb777d44627f
Revises: 166572b78449
Create Date: 2019-03-27 20:51:05.385857

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "cb777d44627f"
down_revision = "166572b78449"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("task", sa.Column("depends_on_id", postgresql.UUID(), nullable=True))
    op.create_foreign_key(
        op.f("fk_task_depends_on_id_task"), "task", "task", ["depends_on_id"], ["id"]
    )


def downgrade():
    op.drop_constraint(op.f("fk_task_depends_on_id_task"), "task", type_="foreignkey")
    op.drop_column("task", "depends_on_id")
