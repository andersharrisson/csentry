"""add task table

Revision ID: 7d0d580cdb1a
Revises: f5a605c0c835
Create Date: 2018-06-28 21:01:13.171328

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "7d0d580cdb1a"
down_revision = "f5a605c0c835"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "task",
        sa.Column("id", postgresql.UUID(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("name", sa.Text(), nullable=False),
        sa.Column("command", sa.Text(), nullable=True),
        sa.Column(
            "status",
            sa.Enum(
                "QUEUED", "FINISHED", "FAILED", "STARTED", "DEFERRED", name="job_status"
            ),
            nullable=True,
        ),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["user_id"], ["user_account.id"], name=op.f("fk_task_user_id_user_account")
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_task")),
    )
    op.create_index(op.f("ix_task_name"), "task", ["name"], unique=False)


def downgrade():
    op.drop_index(op.f("ix_task_name"), table_name="task")
    op.drop_table("task")
    op.execute("DROP TYPE job_status")
