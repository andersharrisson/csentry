"""Rename physical device type

Revision ID: ea606be23b95
Revises: 8f135d5efde2
Create Date: 2018-04-27 19:30:16.398508

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "ea606be23b95"
down_revision = "8f135d5efde2"
branch_labels = None
depends_on = None


def upgrade():
    device_type = sa.sql.table(
        "device_type", sa.sql.column("id"), sa.sql.column("name")
    )
    op.execute(
        device_type.update()
        .where(device_type.c.name == "Physical")
        .values(name="Physical Machine")
    )


def downgrade():
    device_type = sa.sql.table(
        "device_type", sa.sql.column("id"), sa.sql.column("name")
    )
    op.execute(
        device_type.update()
        .where(device_type.c.name == "Physical Machine")
        .values(name="Physical")
    )
