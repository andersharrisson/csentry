"""Add HOSTNAME Ansible group type

Revision ID: b1eda5cb7d9d
Revises: acd72492f46f
Create Date: 2020-03-04 20:37:15.636489

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = "b1eda5cb7d9d"
down_revision = "acd72492f46f"
branch_labels = None
depends_on = None


def upgrade():
    op.execute("COMMIT")
    op.execute("ALTER TYPE ansible_group_type ADD VALUE 'HOSTNAME'")


def downgrade():
    # Removing an individual value from an enum type isn't supported
    # https://www.postgresql.org/docs/current/datatype-enum.html
    pass
