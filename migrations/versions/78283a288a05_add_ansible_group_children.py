"""Add Ansible group children

Revision ID: 78283a288a05
Revises: 924d15deb321
Create Date: 2018-07-27 17:05:57.105899

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "78283a288a05"
down_revision = "924d15deb321"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "ansible_groups_parent_child",
        sa.Column("parent_group_id", sa.Integer(), nullable=False),
        sa.Column("child_group_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["child_group_id"],
            ["ansible_group.id"],
            name=op.f("fk_ansible_groups_parent_child_child_group_id_ansible_group"),
        ),
        sa.ForeignKeyConstraint(
            ["parent_group_id"],
            ["ansible_group.id"],
            name=op.f("fk_ansible_groups_parent_child_parent_group_id_ansible_group"),
        ),
        sa.PrimaryKeyConstraint(
            "parent_group_id",
            "child_group_id",
            name=op.f("pk_ansible_groups_parent_child"),
        ),
    )


def downgrade():
    op.drop_table("ansible_groups_parent_child")
