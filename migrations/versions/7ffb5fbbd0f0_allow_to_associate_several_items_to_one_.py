"""Allow to associate several items to one host

Revision ID: 7ffb5fbbd0f0
Revises: e07c7bc870be
Create Date: 2018-04-20 12:01:25.242815

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "7ffb5fbbd0f0"
down_revision = "e07c7bc870be"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("item", sa.Column("host_id", sa.Integer(), nullable=True))
    op.create_foreign_key(
        op.f("fk_item_host_id_host"), "item", "host", ["host_id"], ["id"]
    )
    op.add_column(
        "item_version",
        sa.Column("host_id", sa.Integer(), autoincrement=False, nullable=True),
    )
    # Fill the item host_id based on the old host item_id value
    conn = op.get_bind()
    res = conn.execute("SELECT id, item_id FROM host WHERE item_id IS NOT NULL")
    results = res.fetchall()
    item = sa.sql.table("item", sa.sql.column("id"), sa.sql.column("host_id"))
    for result in results:
        op.execute(
            item.update().where(item.c.id == result[1]).values(host_id=result[0])
        )
    # We can drop the item_id column now
    op.drop_constraint("fk_host_item_id_item", "host", type_="foreignkey")
    op.drop_column("host", "item_id")


def downgrade():
    op.add_column(
        "host", sa.Column("item_id", sa.INTEGER(), autoincrement=False, nullable=True)
    )
    op.create_foreign_key("fk_host_item_id_item", "host", "item", ["item_id"], ["id"])
    # Fill the host item_id based on the item host_id value
    conn = op.get_bind()
    res = conn.execute("SELECT id, host_id FROM item WHERE host_id IS NOT NULL")
    results = res.fetchall()
    host = sa.sql.table("host", sa.sql.column("id"), sa.sql.column("item_id"))
    for result in results:
        op.execute(
            host.update().where(host.c.id == result[1]).values(item_id=result[0])
        )
    # Drop the unused columns
    op.drop_column("item_version", "host_id")
    op.drop_constraint(op.f("fk_item_host_id_host"), "item", type_="foreignkey")
    op.drop_column("item", "host_id")
