"""Rename machine_type to device_type

Revision ID: e07c7bc870be
Revises: a73eeb144fa1
Create Date: 2018-04-10 12:34:24.426512

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "e07c7bc870be"
down_revision = "a73eeb144fa1"
branch_labels = None
depends_on = None


def upgrade():
    # Rename the table and constraints
    op.rename_table("machine_type", "device_type")
    op.execute("ALTER INDEX pk_machine_type RENAME TO pk_device_type")
    op.execute(
        "ALTER TABLE device_type RENAME CONSTRAINT uq_machine_type_name TO uq_device_type_name"
    )
    # Rename the foreign key in the host table
    op.alter_column(
        "host",
        "machine_type_id",
        new_column_name="device_type_id",
        existing_type=sa.Integer,
    )
    op.drop_constraint(
        "fk_host_machine_type_id_machine_type", "host", type_="foreignkey"
    )
    op.create_foreign_key(
        op.f("fk_host_device_type_id_device_type"),
        "host",
        "device_type",
        ["device_type_id"],
        ["id"],
    )


def downgrade():
    # Rename the table and constraints
    op.rename_table("device_type", "machine_type")
    op.execute("ALTER INDEX pk_device_type RENAME TO pk_machine_type")
    op.execute(
        "ALTER TABLE machine_type RENAME CONSTRAINT uq_device_type_name TO uq_machine_type_name"
    )
    # Rename the foreign key in the host table
    op.alter_column(
        "host",
        "device_type_id",
        new_column_name="machine_type_id",
        existing_type=sa.Integer,
    )
    op.drop_constraint(
        "fk_host_device_type_id_machine_type", "host", type_="foreignkey"
    )
    op.create_foreign_key(
        "fk_host_machine_type_id_machine_type",
        "host",
        "machine_type",
        ["machine_type_id"],
        ["id"],
    )
